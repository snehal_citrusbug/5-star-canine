<?php
/**
 * single-team
 */


get_header();

$post = get_post($post = $postId);
 ?>

        <div class="inside-banner-blk">
			<div class="container">
			<div class="row">
				<div class="col-xl-12 col-sm-12">
					<h2 class="page-title">Our Team</h2>
				</div>
			</div>
			</div>
		</div><!-- end of banner blk -->
	</div><!-- end of banner div -->

    <div class="middle-container">

		<div class="middle-container">

		<section class="intro-about-section">
			<div class="container">

				<div class="intro-about-div">

					<div class="row">

						<div class="col-xl-6 col-sm-12 pull-right" style="order:2">
							<div class="img"><img src="<?php echo get_the_post_thumbnail_url($post->ID); ?>" alt="post" class="img-fluid" /></div>
						</div><!-- end of col -->
						<div class="col-xl-6 col-sm-12 pull-left"  style="order:1">
							<h3 class="title-blk"><?php echo $post->post_title;?></h3>
                            <?php echo apply_filters('the_content',$post->post_content); ?>

						</div><!-- end of col -->

					</div>

				</div><!-- end of intro-about-div -->

			</div>
		</section><!-- end of intro-about-section -->
<?php get_footer();
