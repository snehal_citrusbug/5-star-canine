<?php
/**
 * Template Name: Simple Template
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();

$post = get_post(get_the_ID());

 ?>

        <div class="inside-banner-blk">
			<div class="container">
			<div class="row">
				<div class="col-xl-12 col-sm-12">
					<h2 class="page-title"><?php echo $post->post_title;?></h2>
				</div>
			</div>
			</div>
		</div><!-- end of banner blk -->
	</div><!-- end of banner div -->

    <div class="middle-container">

		<section class="intro-about-section">
			<div class="container">

				<div class="intro-about-div">

					<div class="row">						
						<div class="col-sm-12 pull-left"  style="order:1">
                            <?php echo apply_filters('the_content',$post->post_content); ?>
						</div><!-- end of col -->

					</div>

				</div><!-- end of intro-about-div -->

			</div>
		</section><!-- end of intro-about-section -->


<?php get_footer();
