<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

 <!-- HTML CSS & JS  -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Poppins:300,400,500,600,700,900" rel="stylesheet">

<link href="<?php bloginfo('template_url'); ?>/assets/css/style.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('template_url'); ?>/assets/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="<?php bloginfo('template_url'); ?>/assets/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php bloginfo('template_url'); ?>/assets/js/modernizr.js" type="text/javascript"></script>

 <!-- HTML CSS & JS  -->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="wrapper">
	<header>
		<div class="header-div">
			<div class="header-top-div">
				<div class="container">
				<div class="row">
					<div class="top-phone-div"><a href="tel:<?php echo get_option( 'phoneno' );  ?>" ><i class="fa fa-phone"></i> Call <?php echo get_option( 'phoneno' );  ?></a></div>
					<div class="top-social-div">
						<a href="<?php echo get_option( 'fb_url' );  ?>"><i class="fa fa-facebook"></i></a>
						<a href="<?php echo get_option( 'insta_url' );  ?>"><i class="fa fa-instagram"></i></a>
						<a href="<?php echo get_option( 'twitter_url' );  ?>"><i class="fa fa-twitter"></i></a>
						<a href="<?php echo get_option( 'youtube_url' );  ?>"><i class="fa fa-youtube"></i></a>
					</div>
				</div>
				</div>
			</div><!-- end of header-top-div -->
			<div class="header-nav-logo-div">
				<div class="container">
				<div class="row">
					<div class="logo-div">
						<a href="index.html">
                         <?php the_custom_logo();?>
                        <!-- <img src="<?php bloginfo('template_url'); ?>/assets/images/logo.png" alt="5 star canine" class="img-fluid" /> -->
                        </a>
					</div>


					<div class="nav-m-bar"><a href="#" onclick="openNav()" class="" data-placement="bottom" title="" data-original-title="Menu">
						<i class="menu-bars"></i></a>
					</div>
					<div class="nav-div clearfix" id="mySidenav" >
                        <?php
                        $defaults = array(
                            'theme_location'  => 'primary',
                            'menu'            => 'Top Menu',
                            'container'       => 'ul',
                            'menu_class'      => 'nav',
                            'echo'            => true,
                            'fallback_cb'     => 'wp_page_menu',
                            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                            'depth'           => 0,
                            'walker' => new wp_bootstrap_navwalker
                        );
                        wp_nav_menu( $defaults );
                        ?>

					<!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
						<ul class="nav" id="nav-res">
							<li><a href="index.html" class="active">Home</a></li>
							<li><a href="about.html">About Us</a></li>
							<li><a href="services.html">Services</a></li>
							<li><a href="our-team.html">Team</a></li>
							<li><a href="gallery.html">Gallery</a></li>
							<li><a href="contact.html">Contact Us</a></li>
						</ul> -->
					</div>
				</div>
				</div>
			</div><!-- end fo header-nav-logo-div -->
		</div><!-- end of header-div -->
	</header>
<?php
if (is_front_page()) { ?>
	<div class="banner-div">
<?php }
else { ?>
	<div class="inside-banner-div">
		<!-- <div class="inside-banner-blk">
			<div class="container">
			<div class="row">
				<div class="col-xl-12 col-sm-12">
					<h2 class="page-title">About Us</h2>
				</div>
			</div>
			</div> -->
<?php } ?>