<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
$aboutus = get_post(9);
$contactus = get_post(10);


get_header(); 
    echo do_shortcode('[smartslider3 slider=2]');
    ?>
</div><!-- end of banner div -->

<div class="middle-container">
    <section class="intro-about-section">
        <div class="container">

            <div class="intro-about-div">

                <h2 class="blk-title text-center">About Us</h2>
                <div class="title-bdr"></div>

                <div class="row">

                    <div class="col-xl-6 col-sm-12 pull-right" style="order:2">

                        <div class="img"><img src="<?php echo get_the_post_thumbnail_url($aboutus->ID); ?>" alt="aboutus" class="img-fluid" /></div>
                    </div><!-- end of col -->
                    <div class="col-xl-6 col-sm-12 pull-left"  style="order:1">
                        <?php echo apply_filters('the_content',$aboutus->post_content); ?>
                        <a href="<?php echo site_url(); ?>/about" class="readmore-link">more</a>
                    </div><!-- end of col -->
                </div>

            </div><!-- end of intro-about-div -->

        </div>
    </section><!-- end of intro-about-section -->

    <section class="find-puppies-section">
        <div class="container">

            <div class="find-puppies-div">

                <h2 class="blk-title text-center">Find Puppies</h2>
                <div class="title-bdr"></div>

                <div class="row">
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-1.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-2.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-3.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-4.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-5.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-6.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->

                </div>

                <div class="text-center mt20"><a href="#" class="readmore-link text-center">read more</a></div>

            </div><!-- end of find-puppies-div -->

        </div>
    </section><!-- end of find-puppies-section -->

    <section class="contact-section">
        <div class="container">

        <div class="contact-div">

            <h2 class="blk-title white-txt text-center">Contact Us</h2>
            <div class="title-white-bdr"></div>

            <div class="row">
                <div class="col-xl-6 col-sm-6">
                    <div class="contact-txt-blk">
                        <p><?php echo get_option( 'contactus_content' );  ?></p>
                    </div>
                </div><!-- end of col -->
                <div class="col-xl-6 col-sm-6">
                    <div class="contact-form-div">
                        <div class="clearfix">
                        <?php echo do_shortcode('[contact-form-7 id="51" title="Contact form"]'); ?>
                        </div>
                    </div>
                </div><!-- end of col -->

            </div><!-- end of row -->

        </div><!-- end of contact-div -->
        </div><!-- end of container -->
    </section><!-- edn o fcontact-section -->

    <section class="location-section">

        <div class="location-div">

            <h2 class="blk-title text-center">Location</h2>
            <div class="title-bdr"></div>

                    <!-- <div class="map-div">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23841.422098009327!2d-85.57985407316666!3d41.673504179662935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8816578a01d0427b%3A0x547bef64186fe664!2sShipshewana%2C+IN+46565%2C+USA!5e0!3m2!1sen!2sin!4v1541073600634" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div> -->

                    <div id="map" class="map-div">
                    </div>
                <script>
                    function initMap() {
                        var myLatLng = {lat: <?php echo $contactus->latitude; ?>, lng: <?php echo $contactus->longitude; ?>};
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 16,
                            center: myLatLng
                        });

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            title: 'map'
                        });
                    }
                    </script>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpiWWaPaKhBbPSs2vZBECcZcvH2LUtj2U&callback=initMap"></script>

            </div>

        </div><!-- end of location-div -->
    </section><!-- end of location-section -->

<?php get_footer();
