<?php
/**
 * Template Name: Services
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();


$service = get_post(31);
 ?>

        <div class="inside-banner-blk">
			<div class="container">
			<div class="row">
				<div class="col-xl-12 col-sm-12">
					<h2 class="page-title">Services</h2>
				</div>
			</div>
			</div>
		</div><!-- end of banner blk -->
	</div><!-- end of banner div -->

    <div class="middle-container">

		<section class="intro-about-section service-01">
            <div class="container">
            
                <div class="intro-about-div">
                
                    <div class="row">
                        
                        <div class="col-xl-6 col-sm-12 pull-right" style="order:2">
                            <div class="img"><img src="<?php echo get_the_post_thumbnail_url($service->ID); ?>" alt="service" class="img-fluid service-1" /></div>
                        </div><!-- end of col -->
                        <div class="col-xl-6 col-sm-12 pull-left" style="order:1">
                            <div class="card-left">
                                <h3 class="title-blk"><?php echo $service->header_title;?></h3>
                                <?php echo apply_filters('the_content',$service->post_content); ?>
                            </div>                          
                        </div><!-- end of col -->
                        
                    </div>
            
                </div><!-- end of intro-about-div -->
            
            </div>
        </section><!-- end of service-section -->

        <section class="choose-section clearfix">
            <div class="choose-div clearfix">

                <div class="container ">
                    <div class="row">

                        <div class="heading-top clearfix">
                            <h2 class="blk-title text-center h2-title">why choose us</h2>
                            <div class="title-bdr"></div>
                        </div>

                        <!-- item 1 -->
                        <div class="features col-lg-4 col-sm-4 col-xs-12 clearfix">
                            <div class="col-xs-12 big-icon">
                                <!-- icon -->
                                <i class="flaticon-dog-and-pets-house"></i>
                            </div>
                            <div class="col-xs-12">
                                <div class="thumb-img-1 clearfix">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/bookmark.svg" class="img-fluid img-fluid01" alt="Lowest Price" title="Lowest Price">
                                </div>
                                <h5><?php echo $service->section_title_1;?></h5>
                                <p><?php echo $service->section_desc_1;?></p>
                            </div>
                        </div>
                        <!-- item 2 -->
                        <div class="features col-lg-4 col-sm-4 col-xs-12 clearfix">
                            <div class="col-xs-12 big-icon">
                                <!-- icon -->
                                <i class="flaticon-dog-in-front-of-a-man"></i>
                            </div>
                            <div class="col-xs-12">
                                <div class="thumb-img-1 clearfix">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/shield.svg" class="img-fluid img-fluid01" alt="Healthy Pets" title="Healthy Pets">
                                </div>
                                <h5><?php echo $service->section_title_2;?></h5>
                                <p><?php echo $service->section_desc_2;?></p>
                            </div>
                        </div>
                        <!-- item 3 -->
                        <div class="features col-lg-4 col-sm-4 col-xs-12 clearfix">
                            <div class="col-xs-12 big-icon">
                                <!-- icon -->
                                <i class="flaticon-veterinarian-hospital"></i>
                            </div>
                            <div class="col-xs-12">
                                <div class="thumb-img-1 clearfix">
                                    <img src="<?php bloginfo('template_url'); ?>/assets/images/home.svg" class="img-fluid img-fluid01" alt="Happy People" title="Happy People">
                                </div>
                                <h5><?php echo $service->section_title_3;?></h5>
                                <p><?php echo $service->section_desc_3;?></p>
                            </div>
                        </div>
                        <!-- /col-md-3 -->
                    
                    </div>
                </div>
            </div><!-- end of about-action-div -->

        </section><!-- end of about-action-section -->

        <section class="intro-about-section service-01 service-02">
            <div class="container">
            
                <div class="intro-about-div">
                
                    <div class="row">
                        
                        <div class="col-xl-6 col-sm-12 pull-right" style="order:1">
                            <div class="img-left-div">
                                <?php echo wp_get_attachment_image( 74 ,array(400,400),array( 'alt' => "service" , 'class' => 'img-fluid service-3' ) );?>
                            </div>
                        </div><!-- end of col -->
                        <div class="col-xl-6 col-sm-12 pull-left" style="order:2">
                            <div class="card-left">
                                <h3 class="title-blk"><?php echo $service->contact_section_title;?></h3>
                                <p><?php echo $service->contact_section_desc_1;?></p>
                                <p class="p-title1 bold-600"><?php echo $service->contact_section_desc_2;?></p>
                                <p><a href="<?php echo get_site_url().'/contact'; ?>" class="readmore-link">Contact us</a></p>
                            </div>                          
                        </div><!-- end of col -->
                        
                    </div>
            
                </div><!-- end of intro-about-div -->
            
            </div>
        </section><!-- end of service-section -->
<?php get_footer();
