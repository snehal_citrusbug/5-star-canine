<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
    <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
        <div class="carousel-item active">
            <div class="carousel-caption d-none d-md-block">
            <h5>If you are looking for a puppy that wants to be with you always</h5>
            <p><a href="#">Read more</a></p>
            </div>
            <img class="d-block w-100" src="<?php bloginfo('template_url'); ?>/assets/images/banner-1.jpg" alt="">
        </div>
        <div class="carousel-item">
            <div class="carousel-caption d-none d-md-block">
                <h5>If you are looking for a puppy that wants to be with you always</h5>
                <p><a href="#">Read more</a></p>
                </div>
            <img class="d-block w-100" src="<?php bloginfo('template_url'); ?>/assets/images/banner-1.jpg" alt="">
        </div>
        <div class="carousel-item">
            <div class="carousel-caption d-none d-md-block">
                <h5>If you are looking for a puppy that wants to be with you always</h5>
                <p><a href="#">Read more</a></p>
                </div>
            <img class="d-block w-100" src="<?php bloginfo('template_url'); ?>/assets/images/banner-1.jpg" alt="">
        </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
        </a>
    </div>
</div><!-- end of banner div -->

<div class="middle-container">
    <section class="intro-about-section">
        <div class="container">

            <div class="intro-about-div">

                <h2 class="blk-title text-center">About Us</h2>
                <div class="title-bdr"></div>

                <div class="row">

                    <div class="col-xl-6 col-sm-12 pull-right" style="order:2">
                        <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/about-img.jpg" alt="" class="img-fluid" /></div>
                    </div><!-- end of col -->
                    <div class="col-xl-6 col-sm-12 pull-left"  style="order:1">
                        <p>Lorem ipsum dolor sit amet, purus ac duis dui neque lacinia, pellentesque ultrices nunc elit volutpat, vel in duis penatibus, mus rhoncus, eu diam iaculis. Donec tempus id felis id lectus turpis, aliquet scelerisque. Purus dapibus aliquam. Purus nunc nulla suscipit sociosqu tempor in, wisi ac amet at eros phasellus dolor, imperdiet consequat mi rhoncus fermentum orci, odio in wisi donec. At nam dictumst eros pharetra, id tortor eu eu nisl, mauris ipsum. Nam vel arcu facilisi velit a. Lectus turpis ut vestibulum in fusce</p>

                        <p><a href="#" class="readmore-link">more</a></p>

                    </div><!-- end of col -->


                </div>

            </div><!-- end of intro-about-div -->

        </div>
    </section><!-- end of intro-about-section -->

    <section class="find-puppies-section">
        <div class="container">

            <div class="find-puppies-div">

                <h2 class="blk-title text-center">Find Puppies</h2>
                <div class="title-bdr"></div>

                <div class="row">
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-1.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-2.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-3.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-4.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-5.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-4 col-sm-4">
                        <div class="find-puppies-blk">
                            <a href="#">
                            <div class="img"><img src="<?php bloginfo('template_url'); ?>/assets/images/puppies-6.jpg" alt="" class="img-fluid" /></div>
                            <div class="overlay"><span>read more</span></div>
                            </a>
                        </div>
                    </div><!-- end of col -->

                </div>

                <div class="text-center mt20"><a href="#" class="readmore-link text-center">read more</a></div>

            </div><!-- end of find-puppies-div -->

        </div>
    </section><!-- end of find-puppies-section -->

    <section class="contact-section">
        <div class="container">

        <div class="contact-div">

            <h2 class="blk-title white-txt text-center">Contact Us</h2>
            <div class="title-white-bdr"></div>

            <div class="row">
                <div class="col-xl-6 col-sm-6">
                    <div class="contact-txt-blk">
                        <p>IF YOU NEED TO ANY HELP PLEASE CONTACT US</p>
                    </div>
                </div><!-- end of col -->
                <div class="col-xl-6 col-sm-6">
                    <div class="contact-form-div">
                        <div class="clearfix">
                            <div  class="form-group"><input type="text" class="form-control" placeholder="Your Name" /></div>
                            <div  class="form-group"><input type="email" class="form-control" placeholder="Your Email" /></div>
                            <div  class="form-group"><textarea class="form-control" placeholder="Your Message"></textarea></div>
                            <div  class="form-group"><button class="white-sbtn">Send</button></div>
                        </div>
                    </div>
                </div><!-- end of col -->

            </div><!-- end of row -->

        </div><!-- end of contact-div -->
        </div><!-- end of container -->
    </section><!-- edn o fcontact-section -->

    <section class="location-section">

        <div class="location-div">

            <h2 class="blk-title text-center">Location</h2>
            <div class="title-bdr"></div>

                    <div class="map-div">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d23841.422098009327!2d-85.57985407316666!3d41.673504179662935!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8816578a01d0427b%3A0x547bef64186fe664!2sShipshewana%2C+IN+46565%2C+USA!5e0!3m2!1sen!2sin!4v1541073600634" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>

            </div>

        </div><!-- end of location-div -->
    </section><!-- end of location-section -->
