<?php
/**
 * Template Name: Gallery
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();

$args = array(
    'order'    => 'ASC',
    'post_type'   => 'gallery_post',
    'posts_per_page'   => 500,
	'offset'           => 0,
);
$postArray = get_posts( $args );
 ?>

        <div class="inside-banner-blk">
			<div class="container">
			<div class="row">
				<div class="col-xl-12 col-sm-12">
					<h2 class="page-title">Gallery</h2>
				</div>
			</div>
			</div>
		</div><!-- end of banner blk -->
	</div><!-- end of banner div -->

    <div class="middle-container">

		<section class="gallery-section clearfix">
			<div class="gallery-div clearfix">
				<h2 class="blk-title text-center">Gallery</h2>
				<div class="title-bdr"></div>
				<div class="container">
                <div class="row">
                    <?php
                    foreach ($postArray as $key => $value) { ?>
                        <div class="col-lg-4 col-sm-4 col-xs-12">
							<div class="team-blk-gallery clearfix">
                            <?php
                                if( get_the_post_thumbnail_url($value->ID) != false ) { ?>
                                    <a href="<?php echo get_the_post_thumbnail_url($value->ID); ?>"  data-fancybox>
                                    <div class="img-div"><img src="<?php echo get_the_post_thumbnail_url($value->ID); ?>" alt="gallery-img" class="img-fluid img-gallery" /></div>
									<div class="overlay-div">
										<div class="text-box">
											<img src="<?php bloginfo('template_url'); ?>/assets/images/bone.svg" alt="icon-img" class="img-fluid img-icon" />
										</div>
                                    </div>
                                    </a>
                                    <?php
                                }
                            ?>
                            </div>
                        </div>
                    <?php
                    }
                    ?>
           </div>
			</div><!-- end of about-action-div -->
		</section><!-- end of about-action-section -->

<?php get_footer();
