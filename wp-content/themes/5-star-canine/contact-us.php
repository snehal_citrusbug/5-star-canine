<?php
/**
 * Template Name: Contact Us
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();

$contactus = get_post(10);


 ?>

        <div class="inside-banner-blk">
			<div class="container">
			<div class="row">
				<div class="col-xl-12 col-sm-12">
					<h2 class="page-title">Contact Us</h2>
				</div>
			</div>
			</div>
		</div><!-- end of banner blk -->
	</div><!-- end of banner div -->

    <div class="middle-container">

		<section class="our-team-section">
			<div class="our-team-div">

				<div class="container">
					<div class="row">
						
						<div class="heading-top text-wcenter clearfix">
							<h2 class="blk-title text-center">Contact Us</h2>
							<div class="title-bdr mb-30"></div>
							<p class="text-center"><?php echo get_option( 'contactus_content' );  ?></p>
						</div>
						
						<div class="center-600 clearfix">
							<div class="contact-form-div">
								<div class="clearfix">
									<?php echo do_shortcode('[contact-form-7 id="51" title="Contact form"]'); ?>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div><!-- end of about-action-div -->
		</section>

		<section class="location-section">
		
			<div class="location-div">
			
				<div id="map" class="map-div">
                    </div>
                <script>
                    function initMap() {
                        var myLatLng = {lat: <?php echo $contactus->latitude; ?>, lng: <?php echo $contactus->longitude; ?>};
                        var map = new google.maps.Map(document.getElementById('map'), {
                            zoom: 16,
                            center: myLatLng
                        });

                        var marker = new google.maps.Marker({
                            position: myLatLng,
                            map: map,
                            title: 'map'
                        });
                    }
                    </script>
                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCpiWWaPaKhBbPSs2vZBECcZcvH2LUtj2U&callback=initMap"></script>
					
				</div>
				
			</div><!-- end of location-div -->
		</section><!-- end of location-section -->

<?php get_footer();
