<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */
$args = array(
    'order'    => 'ASC',
    'post_type'   => 'gallery_post',
    'posts_per_page'   => 6,
);
$galleryArray = get_posts( $args );
?>

</div><!-- end of middle-container -->

	<footer>
		<div class="footer-div">
			<div class="container">
                <div class="row">
                    <div class="col-xl-3 col-sm-3">
                        <div class="footer-link-div">
                            <h3>About Us</h3>
                            <p><?php echo get_option( 'aboutus_content' );  ?></p>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-3 col-sm-3">
                        <div class="footer-link-div">
                            <h3>Links</h3>
                            <?php
                                $defaults = array(
                                    'theme_location'  => 'primary',
                                    'menu'            => 'Footer Menu',
                                    'container'       => 'ul',
                                    'menu_class'      => 'footer-ul',
                                    'echo'            => true,
                                    'fallback_cb'     => 'wp_page_menu',
                                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    'depth'           => 0,
                                    'walker' => new wp_bootstrap_navwalker
                                );
                                wp_nav_menu( $defaults );
                            ?>
                            <!-- <ul class="footer-ul">
                                <li><a href="#">Search</a></li>
                                <li><a href="#">Help</a></li>
                                <li><a href="#">Information</a></li>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Shipping details</a></li>
                            </ul> -->
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-3 col-sm-3">
                        <div class="footer-link-div">
                            <h3>Contact Us</h3>
                            <div class="footer-social-div">
                            <a href="<?php echo get_option( 'fb_url' );  ?>"><i class="fa fa-facebook"></i></a>
                            <a href="<?php echo get_option( 'insta_url' );  ?>"><i class="fa fa-instagram"></i></a>
                            <a href="<?php echo get_option( 'twitter_url' );  ?>"><i class="fa fa-twitter"></i></a>
                            <a href="<?php echo get_option( 'youtube_url' );  ?>"><i class="fa fa-youtube"></i></a>
                        </div>
                        </div>
                    </div><!-- end of col -->
                    <div class="col-xl-3 col-sm-3">
                        <div class="footer-link-div">
                            <h3>Gallery</h3>
                            <ul class="footer-photos-ul">
                                <?php
                                foreach ($galleryArray as $key => $value) {
                                    if( get_the_post_thumbnail_url($value->ID) != false ) { ?>
                                        <li><a href="<?php echo get_the_post_thumbnail_url($value->ID); ?>" data-toggle="lightbox" data-gallery="example-gallery" ><img src="<?php echo get_the_post_thumbnail_url($value->ID); ?>"  alt=""  class="img-fluid" /></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    </div><!-- end of col -->

                </div>
			</div>
            <div class="copyright-div">
                <p><?php echo get_option( 'footer_content' );  ?> </p>
            </div>
		</div>
	</footer>
</div>




<!-- HTML CSS & JS  -->
<script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.min.js" type="text/javascript"></script>
<script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js" type="text/javascript"></script>

<link href="<?php bloginfo('template_url'); ?>/assets/css/ekko-lightbox.css" media="all" rel="stylesheet" type="text/css" />
<script src="<?php bloginfo('template_url'); ?>/assets/js/ekko-lightbox.js" type="text/javascript"></script>

<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/assets/js/custom.js"></script>
<!-- HTML CSS & JS  -->
<?php wp_footer(); ?>

</body>
</html>
