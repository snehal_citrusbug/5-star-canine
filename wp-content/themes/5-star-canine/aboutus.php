<?php
/**
 * Template Name: About Us
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header();

$aboutus = get_post(9);
// Team Array
$args = array(
    'order' => 'ASC',
    'post_type' => 'team_post',
    'posts_per_page' => 3,
);
$postArray = get_posts($args);

 ?>

        <div class="inside-banner-blk">
			<div class="container">
			<div class="row">
				<div class="col-xl-12 col-sm-12">
					<h2 class="page-title">About Us</h2>
				</div>
			</div>
			</div>
		</div><!-- end of banner blk -->
	</div><!-- end of banner div -->

    <div class="middle-container">

		<section class="intro-about-section">
			<div class="container">

				<div class="intro-about-div">

					<div class="row">

						<div class="col-xl-6 col-sm-12 pull-right" style="order:2">
							<div class="img"><img src="<?php echo get_the_post_thumbnail_url($aboutus->ID); ?>" alt="aboutus" class="img-fluid" /></div>
						</div><!-- end of col -->
						<div class="col-xl-6 col-sm-12 pull-left"  style="order:1">
							<h3 class="title-blk"><?php echo $aboutus->header_title;?></h3>
                            <?php echo apply_filters('the_content',$aboutus->post_content); ?>
                            <a href="<?php echo $aboutus->request_to_call_link;?>" class="readmore-link">Request a Call</a> <span class="span-call-txt">or Call Us: <a href="tel:<?php echo get_option( 'phoneno' );  ?>"><?php echo get_option( 'phoneno' );  ?></a></span></p>
						</div><!-- end of col -->

					</div>

				</div><!-- end of intro-about-div -->

			</div>
		</section><!-- end of intro-about-section -->


		<section class="about-action-section">
			<div class="about-action-div">

				<div class="container">
				<div class="row">

					<!-- item 1 -->
                  <div class="features col-lg-4 col-sm-4 col-xs-12">
                     <div class="col-xs-12 big-icon">
                        <!-- icon -->
                        <i class="flaticon-dog-and-pets-house"></i>
                     </div>
                     <div class="col-xs-12">
						<div class="thumb-img-1 clearfix">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/money-bag.svg" class="img-fluid img-fluid01" alt="Lowest Price" title="Lowest Price">
						</div>
						<h5><?php echo $aboutus->section_title_1;?></h5>
                        <p><?php echo $aboutus->section_desc_1;?></p>
                     </div>
                  </div>
                  <!-- /col-md-3 -->
                  <!-- item 2 -->
                  <div class="features col-lg-4 col-sm-4 col-xs-12">
                     <div class="col-xs-12 big-icon">
                        <!-- icon -->
                        <i class="flaticon-dog-in-front-of-a-man"></i>
                     </div>
                     <div class="col-xs-12">
						<div class="thumb-img-1 clearfix">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/bones.svg" class="img-fluid img-fluid01" alt="Healthy Pets" title="Healthy Pets">
						</div>
						<h5><?php echo $aboutus->section_title_2;?></h5>
                        <p><?php echo $aboutus->section_desc_2;?></p>
                     </div>
                  </div>
                  <!-- /col-md-3 -->
                  <!-- item 3 -->
                  <div class="features col-lg-4 col-sm-4 col-xs-12">
                     <div class="col-xs-12 big-icon">
                        <!-- icon -->
                        <i class="flaticon-veterinarian-hospital"></i>
                     </div>
                     <div class="col-xs-12">
						<div class="thumb-img-1 clearfix">
							<img src="<?php bloginfo('template_url'); ?>/assets/images/smile.svg" class="img-fluid img-fluid01" alt="Happy People" title="Happy People">
						</div>
                        <h5><?php echo $aboutus->section_title_3;?></h5>
                        <p><?php echo $aboutus->section_desc_3;?></p>
                     </div>
                  </div>
                  <!-- /col-md-3 -->
                  <!-- item 4 -->


				</div>
				</div>


			</div><!-- end of about-action-div -->

		</section><!-- end of about-action-section -->



		<section class="our-team-section">
			<div class="our-team-div">
				<h2 class="blk-title text-center">Our Team</h2>
				<div class="title-bdr"></div>
				<div class="container">
				<div class="row">
                    <?php
                    foreach ($postArray as $key => $value) { ?>
					<div class="col-lg-4 col-sm-4 col-xs-12">
						<div class="team-blk">
							<a href="#" class="link-popup" data-toggle="modal" data-target="#team-<?php echo $key;?>">
								<div class="img"><img src="<?php echo get_the_post_thumbnail_url($value->ID); ?>" alt="" class="img-fluid" /></div>
								<h3><?php echo $value->post_title; ?></h3>
							</a>
						</div>
                    </div>
                    <?php
                    }
                    ?>
				</div>
			</div>
		</div><!-- end of about-action-div -->
	    </section><!-- end of about-action-section -->

<?php get_footer();
